#include <iostream>
#if __linux__
#include <memory.h>
#endif
#include "quick_cal_table.h"

using namespace std;

short g_possibilites[9];

int g_solution_count = 0;
int g_retry_count = 0;

static short getPossibilities(const short* a_snapshot, short a_index) {

    short i;
    short possibleCount = 0;

    short un_possible_list[10] = {0};

    // check row
    short row = row_table[a_index];

    for (i = (row - 1) * 9; i < (row - 1) * 9 + 9; ++i) {
        if (a_snapshot[i] > 0) {
            un_possible_list[a_snapshot[i]] = 1;
        }
    }

    // check column
    short col = col_table[a_index];

    for (i = col - 1; i < 81; i += 9) {
        if (a_snapshot[i] > 0) {
            un_possible_list[a_snapshot[i]] = 1;
        }
    }

    // check partition
    short par = par_table[a_index];

    for (i = par; i < par + 21; i ++) {
        if (par_table[i] == par && a_snapshot[i] > 0)
        {
            un_possible_list[a_snapshot[i]] = 1;
        }
    }

    for (i = 1; i < 10; ++i) {
        if (un_possible_list[i] == 0) {
            g_possibilites[possibleCount++] = i;
        }
    }

    return possibleCount;

}

static void guessSolutions(const short* a_snapshot, short a_index) {

    if (a_index > 80) {
        short last_row = 1;

        g_solution_count ++;

        printf("retry count = %d\n",g_retry_count);
        printf("\n----solution %d----\n",g_solution_count);

        for (int k = 0; k < 81; ++k) {
            short row = row_table[k];

            if (row != last_row) {
                printf("\n");
                last_row = row;
            }

            printf("%d,",a_snapshot[k]);
        }

        return;
    }

    for (int j = a_index; j < 81; ++j) {
        if (a_snapshot[j] > 0) {
            return guessSolutions(a_snapshot, j + 1);
        }

        short possibleCount = getPossibilities(a_snapshot, j);

        if (possibleCount == 0) {
            g_retry_count ++;
            return;
        }
        else {

            short* snapshot = new short[81];
            memcpy(snapshot, a_snapshot, 81 * sizeof(short));

            short* possibilites = new short[possibleCount];
            memcpy(possibilites, g_possibilites, possibleCount * sizeof(short));

            short i;

            for (i = 0; i < possibleCount; ++i) {
                snapshot[j] = possibilites[i];
                guessSolutions(snapshot, j + 1);
            }

            delete[] snapshot;
            delete[] possibilites;

            if (i == possibleCount) {
                return;
            }
        }
    }


}

int main() {

    short sudoku_table0[]= {0,0,0, 4,0,0, 0,0,1,// 0~8
                            0,0,8, 0,0,6, 2,3,0,// 9~17
                            3,9,0, 0,0,0, 0,0,0,// 18~26
                            0,0,6, 8,0,9, 0,0,0,// 27~35
                            0,0,0, 2,0,5, 0,1,0,// 36~44
                            0,4,0, 0,7,0, 8,0,0,// 45~53
                            0,1,3, 7,0,0, 0,5,0,// 54~62
                            2,0,0, 0,0,8, 0,0,0,// 63~71
                            0,0,0, 0,0,0, 1,0,9};//72 ~80

    short sudoku_table1[]= {0,0,0, 0,0,3, 0,0,0,
                            0,0,3, 0,7,0, 0,0,0,
                            0,0,5, 0,0,2, 4,0,0,

                            0,0,0, 8,9,0, 7,5,0,
                            5,6,0, 1,0,0, 0,2,0,
                            4,0,0, 0,0,0, 0,0,8,

                            2,0,0, 0,0,0, 3,9,0,
                            0,4,0, 5,8,0, 1,0,0,
                            0,0,0, 0,2,0, 0,0,0};

    guessSolutions(sudoku_table1,0);

    return 0;
}